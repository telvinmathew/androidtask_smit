package com.example.qbuser.androiddevtask.core.networkLayer;

/**
 * Created by Telvin Mathew on 05/02/19.
 */
public class NetworkConstant {
    public static final String GET_SPECIES_ENDPOINT = "species";
    public static final String QUERY_PARAM_PAGE = "page";
}
