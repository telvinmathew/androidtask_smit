package com.example.qbuser.androiddevtask.adapters;

import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.qbuser.androiddevtask.R;
import com.example.qbuser.androiddevtask.databinding.ItemSpeciesBinding;
import com.example.qbuser.androiddevtask.model.data.Species;
import com.example.qbuser.androiddevtask.utils.Constants;

import java.util.List;

/**
 * Created by Telvin Mathew on 05/02/19.
 */
public class SpeciesListAdapter extends RecyclerView.Adapter<SpeciesListAdapter.ViewHolder> {
    private List<Species> speciesList;

    public SpeciesListAdapter(List<Species> speciesList) {
        this.speciesList = speciesList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        ItemSpeciesBinding itemSpecies = DataBindingUtil.inflate(
                LayoutInflater.from(parent.getContext()),
                R.layout.item_species, parent, false);
        return new ViewHolder(itemSpecies);
    }


    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int position) {
        Species species = speciesList.get(position);
        viewHolder.itemSpeciesBinding.setSpecies(species);

        viewHolder.itemView.setTag(position);
        viewHolder.itemView.setOnClickListener((view) -> {
            int clickPosition = (int) view.getTag();
            Species selectedSpecies = speciesList.get(clickPosition);
            if (TextUtils.isEmpty(selectedSpecies.getStatus()) || selectedSpecies.getStatus().equals(view.getContext().getString(R.string.extinct))) {
                selectedSpecies.setStatus(view.getContext().getString(R.string.active));
            } else {
                selectedSpecies.setStatus(view.getContext().getString(R.string.extinct));
            }
            notifyItemChanged(clickPosition);
        });
    }

    @Override
    public int getItemCount() {
        return speciesList.size();
    }

    public void updateData(List<Species> speciesList) {
        int previousSize = this.speciesList.size();
        this.speciesList = speciesList;
        notifyItemChanged(previousSize, speciesList.size()-1);
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        private ItemSpeciesBinding itemSpeciesBinding;

        public ViewHolder(ItemSpeciesBinding itemSpeciesBinding) {
            super(itemSpeciesBinding.getRoot());
            this.itemSpeciesBinding = itemSpeciesBinding;
        }

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
        }
    }
}
