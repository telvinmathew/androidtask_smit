package com.example.qbuser.androiddevtask.view;

import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.qbuser.androiddevtask.R;
import com.example.qbuser.androiddevtask.adapters.SpeciesListAdapter;
import com.example.qbuser.androiddevtask.model.data.Species;
import com.example.qbuser.androiddevtask.viewModel.SpeciesViewModel;

import java.util.ArrayList;
import java.util.List;

public class SpeciesListActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener {

    private RecyclerView rvSpeciesList;
    private ProgressBar progressBar;
    private TextView tvNoDataMsg;
    private SwipeRefreshLayout swipeRefreshLayout;
    private SpeciesViewModel mViewModel;
    private LinearLayoutManager linearLayoutManager;
    private SpeciesListAdapter speciesListAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mViewModel = ViewModelProviders.of(this).get(SpeciesViewModel.class);
        initViews();
        setSpeciesListObserver();
    }

    /**
     * Observing species list LiveData for change in state
     */
    private void setSpeciesListObserver() {
        mViewModel.getmSpeciesListLiveData().observe(this, speciesList -> {
            swipeRefreshLayout.setRefreshing(false);
            updateUI(speciesList);
        });
    }

    /**
     * Update UI on species list
     *
     * @param speciesList
     */
    private void updateUI(List<Species> speciesList) {
        if (speciesList != null && speciesList.size() > 0) {
            rvSpeciesList.setVisibility(View.VISIBLE);
            tvNoDataMsg.setVisibility(View.GONE);
            speciesListAdapter.updateData(speciesList);
            swipeRefreshLayout.setEnabled(false);
        } else {
            rvSpeciesList.setVisibility(View.GONE);
            tvNoDataMsg.setVisibility(View.VISIBLE);
            tvNoDataMsg.setText(getString(R.string.empty_species_msg));
            swipeRefreshLayout.setEnabled(true);
        }
        // remove loading ui
        progressBar.setVisibility(View.GONE);
        mViewModel.setIsLoading(false);
    }

    /**
     * Initialize views
     */
    private void initViews() {
        // Support Action bar
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //Progress bar
        progressBar = findViewById(R.id.progressBar);

        // Recylerview
        rvSpeciesList = findViewById(R.id.rvSpecies);

        // Layout Manager
        linearLayoutManager = new LinearLayoutManager(this);
        rvSpeciesList.setLayoutManager(linearLayoutManager);

        // init recyclerview Adapter with empty data
        speciesListAdapter = new SpeciesListAdapter(new ArrayList<>());
        rvSpeciesList.setAdapter(speciesListAdapter);

        // Pagination
        rvSpeciesList.addOnScrollListener(recyclerViewOnScrollListener);

        //Swipelayout
        swipeRefreshLayout = findViewById(R.id.swipe_container);
        swipeRefreshLayout.setOnRefreshListener(this);

        //Message text
        tvNoDataMsg = findViewById(R.id.tvMsg);
    }

    @Override
    public void onRefresh() {
        // Initail swipe to refresh update
        mViewModel.updateSpeciesList();
    }

    // On Scroll listener to handle pagination
    private RecyclerView.OnScrollListener recyclerViewOnScrollListener = new RecyclerView.OnScrollListener() {
        @Override
        public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
            super.onScrollStateChanged(recyclerView, newState);
        }

        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);
            int visibleItemCount = linearLayoutManager.getChildCount();
            int totalItemCount = linearLayoutManager.getItemCount();
            int firstVisibleItemPosition = linearLayoutManager.findFirstVisibleItemPosition();

            if (!mViewModel.getIsLoading() && !mViewModel.getIsLastPage().getValue()) {
                if ((visibleItemCount + firstVisibleItemPosition) >= totalItemCount
                        && firstVisibleItemPosition >= 0) {
                    mViewModel.incrementPageNumber();
                    mViewModel.updateSpeciesList();
                    progressBar.setVisibility(View.VISIBLE);

                }
            }
        }
    };
}
