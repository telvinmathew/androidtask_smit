package com.example.qbuser.androiddevtask.core.networkLayer;

import com.example.qbuser.androiddevtask.BuildConfig;
import com.example.qbuser.androiddevtask.model.response.SpeciesResponse;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Telvin Mathew on 05/02/19.
 */
public class NetworkServicesImpl implements INetworkServices {

    //Singleton instance
    private static volatile NetworkServicesImpl sInstance;

    //Retrofit instance
    private Retrofit mRetrofit;

    //APIServices instance
    APIServices mAPIServices;

    /**
     * Get Shared instance of NetworkService
     * @return
     */
    public static INetworkServices getInstance() {
        if (null == sInstance) {
            synchronized (NetworkServicesImpl.class) {
                sInstance = new NetworkServicesImpl();
            }
        }
        return sInstance;
    }

    /**
     * Constructor to create Retrofit and APIService instance
     */
    private NetworkServicesImpl() {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();

        mRetrofit = new Retrofit.Builder()
                .baseUrl(BuildConfig.BASE_URL)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create()).build();
        mAPIServices = mRetrofit.create(APIServices.class);
    }

    @Override
    public void getSpecies(int page, Callback<SpeciesResponse> callback) {
        Call<SpeciesResponse> call = mAPIServices
                .getUser(page);
        call.enqueue(callback);
    }
}
