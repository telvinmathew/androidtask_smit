package com.example.qbuser.androiddevtask.core;

import android.content.Context;

import com.example.qbuser.androiddevtask.core.networkLayer.INetworkServices;
import com.example.qbuser.androiddevtask.core.networkLayer.NetworkServicesImpl;

/**
 * Created by Telvin Mathew on 05/02/19.
 */
public class DataManager {

    private static volatile DataManager sInstance;


    // Network instance
    private INetworkServices mNetworkServices;


    /**
     * @param application
     * @return Singleton instance of the ServiceHolder class
     */
    public static DataManager getInstance(Context application){
        if(null == sInstance){
            synchronized (DataManager.class){
                sInstance = new DataManager(application);
            }
        }

        return sInstance;
    }


    private DataManager(Context application){
        mNetworkServices = NetworkServicesImpl.getInstance();
    }


    /**
     * @return Singleton instance of the {@Link INetworkServices} for interactors to make API calls
     */
    public INetworkServices getNetworkServices(){
        return mNetworkServices;
    }

}
