package com.example.qbuser.androiddevtask.core.networkLayer;

import com.example.qbuser.androiddevtask.model.response.SpeciesResponse;

import retrofit2.Callback;


/**
 * Created by Telvin Mathew on 05/02/19.
 */
public interface INetworkServices {

    void getSpecies(int page, Callback<SpeciesResponse> callback);
}
