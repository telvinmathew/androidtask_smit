package com.example.qbuser.androiddevtask.repository;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;

import com.example.qbuser.androiddevtask.core.DataManager;
import com.example.qbuser.androiddevtask.core.networkLayer.INetworkServices;
import com.example.qbuser.androiddevtask.model.data.Species;
import com.example.qbuser.androiddevtask.model.response.SpeciesResponse;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Telvin Mathew on 05/02/19.
 */
public class SpeciesRepository {
    private MutableLiveData<List<Species>> speciesListLiveData;
    private MutableLiveData<Boolean> isLastPage;
    private INetworkServices networkServices;

    public SpeciesRepository(Application application) {
        networkServices = DataManager.getInstance(application).getNetworkServices();
        speciesListLiveData = new MutableLiveData<>();
        isLastPage = new MutableLiveData<>();
    }

    public LiveData<List<Species>> getSpeciesList() {
        return speciesListLiveData;
    }

    public void fetchSpeciesList(int page) {
        networkServices.getSpecies(page, new Callback<SpeciesResponse>() {
            @Override
            public void onResponse(Call<SpeciesResponse> call, Response<SpeciesResponse> response) {
                List<Species> latestSpeciesList = speciesListLiveData.getValue();
                if (latestSpeciesList == null)
                    latestSpeciesList = new ArrayList<>();
                if (response != null && response.body() != null) {
                    SpeciesResponse speciesResponse = response.body();
                    List<Species> results = speciesResponse.getResults();
                    if (results != null && results.size() > 0) {
                        latestSpeciesList.addAll(results);
                    }
                    isLastPage.postValue(false);
                } else {
                    isLastPage.postValue(true);
                }
                speciesListLiveData.postValue(latestSpeciesList);
            }

            @Override
            public void onFailure(Call<SpeciesResponse> call, Throwable t) {
                speciesListLiveData.postValue(speciesListLiveData.getValue());
            }
        });
    }

    public LiveData<Boolean> geIsLastPage() {
        return isLastPage;
    }
}
