package com.example.qbuser.androiddevtask.core.networkLayer;

import com.example.qbuser.androiddevtask.model.response.SpeciesResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by Telvin Mathew on 05/02/19.
 */
public interface APIServices {

    @GET(NetworkConstant.GET_SPECIES_ENDPOINT)
    Call<SpeciesResponse> getUser(@Query(NetworkConstant.QUERY_PARAM_PAGE) int page);

}
