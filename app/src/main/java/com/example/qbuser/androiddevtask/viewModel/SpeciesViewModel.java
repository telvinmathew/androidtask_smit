package com.example.qbuser.androiddevtask.viewModel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.support.annotation.NonNull;

import com.example.qbuser.androiddevtask.model.data.Species;
import com.example.qbuser.androiddevtask.repository.SpeciesRepository;

import java.util.List;

/**
 * Created by Telvin Mathew on 05/02/19.
 */
public class SpeciesViewModel extends AndroidViewModel {

    private LiveData<List<Species>> mSpeciesListLiveData;
    private SpeciesRepository mRepository;
    private int currentPage = 1;
    private boolean isLoading;
    private LiveData<Boolean> isLastPage;

    public SpeciesViewModel(@NonNull Application application) {
        super(application);
        mRepository = new SpeciesRepository(application);
        mSpeciesListLiveData = mRepository.getSpeciesList();
        isLastPage = mRepository.geIsLastPage();
    }

    public void incrementPageNumber() {
        currentPage++;
    }

    public void updateSpeciesList() {
        isLoading = true;
        mRepository.fetchSpeciesList(currentPage);
    }

    public LiveData<List<Species>> getmSpeciesListLiveData() {
        return mSpeciesListLiveData;
    }

    public int getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(int currentPage) {
        this.currentPage = currentPage;
    }

    public boolean getIsLoading() {
        return isLoading;
    }

    public void setIsLoading(boolean isLoading) {
        this.isLoading = isLoading;
    }

    public LiveData<Boolean> getIsLastPage() {
        return isLastPage;
    }
}
